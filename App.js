import React from 'react';
import { Root } from 'native-base';
import AppNavContainer from './src/routes/routes';
import useDataFunnel from './src/services/hooks-services';
import AppContext from './src/services/context-services'

export default function App() {
  const contextHook = useDataFunnel();

  return (
    <AppContext.Provider value={contextHook}>
      <Root>
        <AppNavContainer />
      </Root>
    </AppContext.Provider>
  );
}
