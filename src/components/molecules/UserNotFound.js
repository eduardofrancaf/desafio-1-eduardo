import React from 'react';
import { StyleSheet, Image, View, Text } from 'react-native';

export default function UserNotFound() {
  return (
    <View style={styles.container}>
      <Image
        style={styles.image}
        source={require('../../../assets/octocat.png')}
      />
      <Text style={styles.text}>Está meio vazio por aqui!</Text>
      <Text style={styles.text}>Busque por um usuário</Text>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },

  image: {
    height: 150,
    width: 150,
  },

  text: {
    color: '#898383',
  },
});
