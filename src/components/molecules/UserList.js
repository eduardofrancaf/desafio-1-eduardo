import React from 'react';
import {
  Text,
  View,
  FlatList,
  Image,
  StyleSheet,
  TouchableOpacity,
} from 'react-native';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';

export default function UserList({ list, icon, callback }) {
  const ItemSeparatorComponent = () => <View style={styles.separator} />;
  const renderItem = ({ item }) => {
    return (
      <View style={styles.container}>
        <View style={styles.secondaryContainer}>
          <Image style={styles.image} source={{ uri: item.avatar_url }} />
          <Text style={styles.text}>{item.login}</Text>
        </View>
        <TouchableOpacity
          style={styles.touchableArea}
          onPress={() =>
            callback({
              id: item.id,
              login: item.login,
              avatar_url: item.avatar_url,
            })
          }
        >
          {icon.iconName === 'chevron-right' ? (
            <FontAwesomeIcon icon={icon} size={10} color={'#898383'} />
          ) : (
            <FontAwesomeIcon icon={icon} size={20} color={'red'} />
          )}
        </TouchableOpacity>
      </View>
    );
  };

  return (
    <FlatList
      ItemSeparatorComponent={ItemSeparatorComponent}
      getItemLayout={(data, index) => ({
        length: 55,
        offset: 65 * index,
        index,
      })}
      style={styles.list}
      contentContainerStyle={styles.contentList}
      data={list}
      renderItem={renderItem}
      keyExtractor={(item, index) => index.toString()}
    />
  );
}

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    padding: 5,
    paddingRight: 20,
    width: '98%',
  },
  secondaryContainer: { flexDirection: 'row', alignItems: 'center' },
  touchableArea: {
    height: 30,
    width: 50,
    alignItems: 'center',
    justifyContent: 'center',
  },
  separator: { height: 10 },
  image: { height: 50, width: 50, borderRadius: 10 },
  list: { marginTop: 10 },
  contentList: { paddingBottom: 40 },
  text: { marginLeft: 20, color: '#898383' },
});
