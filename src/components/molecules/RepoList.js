import React from 'react';
import {
  Text,
  View,
  FlatList,
  ActivityIndicator,
  StyleSheet,
  TouchableOpacity,
} from 'react-native';
import * as Linking from 'expo-linking';
import * as Services from '../../services/shared-services';
import AppContext from '../../services/context-services';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import { faFolder } from '@fortawesome/free-solid-svg-icons';
import { faChevronRight } from '@fortawesome/free-solid-svg-icons';

export default function RepoList() {
  const context = React.useContext(AppContext);
  const [repoList, setRepoList] = React.useState([]);
  const [loading, setLoading] = React.useState(false);
  React.useEffect(() => {
    setLoading(true);
    Services.getUserRepo(context.user.login)
      .then((response) => {
        setRepoList(response);
        setLoading(false);
      })
      .catch((error) => {
        console.log(error);
        setLoading(false);
      });
  }, []);

  const ItemSeparatorComponent = () => <View style={styles.separator} />;
  const renderItem = ({ item }) => {
    return (
      <View style={styles.container}>
        <View style={styles.secondaryContainer}>
          <FontAwesomeIcon icon={faFolder} size={50} color={'#018AFF'} />
          <Text style={styles.text}>{item.name}</Text>
        </View>
        <TouchableOpacity
          style={styles.touchableArea}
          onPress={() => {
            Linking.openURL(
              `https://github.com/${context.user.login}/${item.name}`
            );
          }}
        >
          <FontAwesomeIcon icon={faChevronRight} size={10} color={'#898383'} />
        </TouchableOpacity>
      </View>
    );
  };

  if (loading) {
    return (
      <ActivityIndicator
        size="large"
        color="#018AFF"
        style={styles.activityIndicator}
      />
    );
  } else {
    return (
      <FlatList
        ItemSeparatorComponent={ItemSeparatorComponent}
        getItemLayout={(data, index) => ({
          length: 55,
          offset: 65 * index,
          index,
        })}
        style={styles.list}
        contentContainerStyle={styles.contentList}
        data={repoList}
        renderItem={renderItem}
        keyExtractor={(item, index) => index.toString()}
      />
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 10,
    paddingRight: 20,
    alignItems: 'center',
    paddingHorizontal: 5,
    width: '98%',
  },
  touchableArea: {
    height: 30,
    width: 50,
    alignItems: 'center',
    justifyContent: 'center',
  },
  secondaryContainer: { flexDirection: 'row', alignItems: 'center' },
  separator: { height: 10 },
  list: { marginTop: 10 },
  contentList: { paddingBottom: 40 },
  text: { marginLeft: 20, color: '#898383' },
  activityIndicator: { marginTop: 20 },
});
