import React from 'react';
import { StyleSheet, View } from 'react-native';
import { faSearch } from '@fortawesome/free-solid-svg-icons';
import InputText from '../atoms/InputText';
import CommonButton from '../atoms/CommonButton';

export default function InputSearch({ searchUsers }) {
  const [userName, setUserName] = React.useState();
  return (
    <View style={styles.container}>
      <InputText
        userName={userName}
        setUserName={setUserName}
        onChange={(text) => {
          setUserName(text);
        }}
        placeHolder="Buscar usuário"
      />
      <CommonButton
        onPress={() => {
          userName ? searchUsers(userName) : null;
          setUserName();
        }}
        colorIcon={'#FFFFFF'}
        colorButton={'#018AFF'}
        icon={faSearch}
        iconSize={30}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
});
