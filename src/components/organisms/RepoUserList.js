import { View, Text, StyleSheet, TouchableOpacity } from 'react-native';
import React from 'react';
import AppContext from '../../services/context-services';
import { storeData, getData } from '../../services/storage-services';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import { faHeart } from '@fortawesome/free-solid-svg-icons';
import RepoList from '../molecules/RepoList';

export default function RepoUserList() {
  const context = React.useContext(AppContext);
  const [liked, setLiked] = React.useState(false);

  React.useEffect(() => {
    getData('@liked_usersKey')
      .then((response) => {
        context.setLikedUsers(response);
        response.find((item) => item.id === context.user.id)
          ? setLiked(true)
          : setLiked(false);
      })
      .catch((error) => {console.log(error)});
  }, []);

  const like = () => {
    if (!liked) {
      storeData(
        '@liked_usersKey',
        JSON.stringify([...context.likedUsers, context.user])
      );
      context.setLikedUsers([...context.likedUsers, context.user]);
    } else {
      storeData(
        '@liked_usersKey',
        JSON.stringify(
          context.likedUsers.filter((user) => user.id !== context.user.id)
        )
      );
      context.setLikedUsers(
        context.likedUsers.filter((user) => user.id !== context.user.id)
      );
    }
    setLiked(!liked);
  };

  return (
    <View>
      <View style={styles.container}>
        <Text style={styles.text}>
          {!liked ? 'Favoritar' : 'Desfavoritar'} {context.user.login}?
        </Text>
        <TouchableOpacity onPress={like} style={styles.heartContainer}>
          <FontAwesomeIcon
            icon={faHeart}
            size={20}
            color={liked ? 'red' : '#898383'}
          />
        </TouchableOpacity>
      </View>
      <RepoList />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  heartContainer: {
    padding: 10,
    borderRadius: 50,
    backgroundColor: '#E0E2E3',
  },
  text: {
    color: '#898383',
  },
});
