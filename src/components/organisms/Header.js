import React from 'react';
import { StyleSheet, View } from 'react-native';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import { faGithub } from '@fortawesome/free-brands-svg-icons';

export default function Header() {
  return (
    <View style={styles.container}>
      <FontAwesomeIcon icon={faGithub} size={150} color={'#898383'} />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#040404',
    width: '100%',
    paddingVertical:10,
    paddingHorizontal: 20,
    alignItems: 'flex-end'
  },
});
