import React from 'react';
import { StyleSheet, Text, View, ActivityIndicator } from 'react-native';
import * as Services from '../../services/shared-services';
import AppContext from '../../services/context-services';
import InputSearch from '../molecules/InputSearch';
import UserList from '../molecules/UserList';
import UserNotFound from '../molecules/UserNotFound';
import { faChevronRight } from '@fortawesome/free-solid-svg-icons';

export default function Search() {
  const context = React.useContext(AppContext);
  const [loading, setLoading] = React.useState(false);

  const searchUsers = (userName) => {
    setLoading(true);
    Services.getUser(userName)
      .then((response) => {
        context.setUsers(response.items);
        setLoading(false);
      })
      .catch((error) => {
        console.log(error);
        setLoading(false);
      });
  };

  const selectUser = (user) => {
    context.setUser(user);
    context.setSelector(1);
  };

  return (
    <View style={styles.container}>
      <InputSearch searchUsers={searchUsers} />
      <View style={styles.container}>
        {loading ? (
          <ActivityIndicator
            size="large"
            color="#018AFF"
            style={styles.activityIndicator}
          />
        ) : context.users.length === 0 ? (
          <UserNotFound />
        ) : (
          <View style={{ marginTop: 20 }}>
            <Text style={styles.text}>Usuários encontrados</Text>
            <UserList
              list={context.users}
              callback={selectUser}
              icon={faChevronRight}
            />
          </View>
        )}
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  text: { color: '#898383' },
  activityIndicator: { marginTop: 20 },
});
