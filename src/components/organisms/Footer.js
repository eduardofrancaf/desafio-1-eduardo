import React from 'react';
import { StyleSheet, View } from 'react-native';
import AppContext from '../../services/context-services'
import { faSearch } from '@fortawesome/free-solid-svg-icons';
import { faHeart } from '@fortawesome/free-solid-svg-icons';
import CommonButton from '../atoms/CommonButton'

export default function Footer() {
  const context = React.useContext(AppContext)
  return (
    <View style={styles.container}>
      <CommonButton
        onPress={() => {
          context.setSelector(0);
        }}
        colorIcon={'#898383'}
        icon={faSearch}
        iconSize={30}
      />
      <CommonButton
        onPress={() => {
          context.setSelector(2);
        }}
        colorIcon={'#898383'}
        icon={faHeart}
        iconSize={30}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    paddingHorizontal: 20,
    justifyContent: 'space-between',
    backgroundColor: '#F5F5F5',
    width: '100%',
  },
});
