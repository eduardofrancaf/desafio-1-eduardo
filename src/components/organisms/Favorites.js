import { View, Text, StyleSheet } from 'react-native';
import React from 'react';
import AppContext from '../../services/context-services';
import { storeData, getData } from '../../services/storage-services';
import { faTrashAlt } from '@fortawesome/free-solid-svg-icons';
import UserList from '../molecules/UserList';

export default function Favorites() {
  const context = React.useContext(AppContext);

  React.useEffect(() => {
    getData('@liked_usersKey')
      .then((response) => {
        context.setLikedUsers(response);
      })
      .catch((error) => {
        console.log(error);
      });
  }, []);

  const unLikeUser = ({ id }) => {
    storeData(
      '@liked_usersKey',
      JSON.stringify(context.likedUsers.filter((user) => user.id !== id))
    );
    context.setLikedUsers(context.likedUsers.filter((user) => user.id !== id));
  };

  return (
    <View>
      <Text style={styles.text}>Meus Favoritos</Text>
      {context.likedUsers.length === 0 ? (
        <Text style={styles.text2}>Não há favoritos!</Text>
      ) : (
        <UserList
          list={context.likedUsers}
          callback={unLikeUser}
          icon={faTrashAlt}
        />
      )}
    </View>
  );
}

const styles = StyleSheet.create({
  text: {
    color: '#898383',
  },
  text2: {
    marginTop: 40,
    fontSize: 20,
    textAlign: 'center',
    color: '#018AFF',
  },
});
