import React from 'react';
import { StyleSheet, TextInput } from 'react-native';

export default function InputText({ onChange, placeHolder, userName }) {
  return (
    <TextInput
      value={userName}
      onChangeText={onChange}
      placeholder={placeHolder}
      style={styles.inputStyle}
    />
  );
}

const styles = StyleSheet.create({
  inputStyle: {
    height: 45,
    width: '65%',
    paddingLeft: 10,
    borderWidth: 1,
    borderColor: '#D3D3D3',
    borderRadius: 5
  },
});
