import React from 'react';
import {
  StyleSheet,
  TouchableOpacity,
} from 'react-native';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';

export default function CommonButton({
  onPress,
  colorIcon,
  colorButton,
  icon,
  iconSize,
}) {
  const buttonStyle = StyleSheet.flatten({
    backgroundColor: colorButton,
    paddingVertical: 12,
    paddingHorizontal: 25,
    borderRadius: 5,
  });
  return (
    <TouchableOpacity style={buttonStyle} onPress={onPress}>
      <FontAwesomeIcon icon={icon} size={iconSize} color={colorIcon} />
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({});
