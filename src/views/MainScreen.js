import React from 'react';
import {
  View,
  StyleSheet,
  KeyboardAvoidingView,
  Platform,
  Text,
} from 'react-native';
import Header from '../components/organisms/Header';
import Footer from '../components/organisms/Footer';
import Search from '../components/organisms/Search';
import RepoUserList from '../components/organisms/RepoUserList';
import Favorites from '../components/organisms/Favorites';
import AppContext from '../services/context-services';

export default function MainScreen() {
  const context = React.useContext(AppContext);
  return (
    <KeyboardAvoidingView
      behavior={Platform.OS == 'ios' ? 'padding' : 'height'}
      enabled={false}
      style={styles.container}
    >
      <Header />
      <View style={styles.body}>
        {context.selector === 0 ? <Search /> : null}
        {context.selector === 1 ? <RepoUserList /> : null}
        {context.selector === 2 ? <Favorites /> : null}
      </View>
      <Footer />
    </KeyboardAvoidingView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 20,
    backgroundColor: '#040404',
    alignItems: 'center',
  },

  body: {
    flex: 1,
    width: '100%',
    paddingTop: 30,
    paddingHorizontal: 20,
    backgroundColor: '#FDFDFD',
    borderTopStartRadius: 20,
    borderTopEndRadius: 20,
  },
});
