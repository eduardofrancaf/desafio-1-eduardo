import axios from 'axios'

export const getUser = (userName) => {
  return new Promise((resolve, reject) => {
    axios({
      loader: true,
      url: `https://api.github.com/search/users?q=${userName}`,
      method: 'get',
      headers: {
        'Content-Type': 'application/json',
      },
    })
      .then((response) => {
        resolve(response.data);
      })
      .catch((error) => {
        reject(error);
      });
  });
};

export const getUserRepo = (userName) => {
  return new Promise((resolve, reject) => {
    axios({
      loader: true,
      url: `https://api.github.com/users/${userName}/repos`,
      method: 'get',
      headers: {
        'Content-Type': 'application/json',
      },
    })
      .then((response) => {
        resolve(response.data);
      })
      .catch((error) => {
        reject(error);
      });
  });
};
