import { useState } from 'react';

export default function useDataFunnel() {
  const [likedUsers, setLikedUsers] = useState([]);
  const [users, setUsers] = useState([]);
  const [user, setUser] = useState();
  const [selector, setSelector] = useState(0);

  return {
    likedUsers,
    setLikedUsers,
    users,
    setUsers,
    user,
    setUser,
    selector,
    setSelector,
  };
}
