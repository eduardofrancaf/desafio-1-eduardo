import { createStackNavigator } from 'react-navigation-stack';
import { createAppContainer } from 'react-navigation';
import MainScreen from '../views/MainScreen';


const StackNavigatorConfig = {
  initialRouteName: 'Main',
  header: null,
  headerMode: 'none',
};

const RouteConfigs = {
  Main: {
    screen: MainScreen,
  },
};

const AppStackNavigator = createStackNavigator(
  RouteConfigs,
  StackNavigatorConfig
);

const AppNavContainer = createAppContainer(AppStackNavigator);

export default AppNavContainer;
